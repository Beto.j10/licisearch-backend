package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4"
	"golang.org/x/crypto/acme/autocert"
	"golang.org/x/sync/errgroup"
)

func main() {

	isDev := os.Getenv("GO_ENV") == "development"
	//Enable CORS
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowCredentials: true,
		AllowHeaders:     []string{"Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "accept", "origin", "Cache-Control", "X-Requested-With"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "OPTIONS", "GET", "DELETE"},
	}))
	r.GET("/search", search)

	if isDev {
		log.Println("LiciSearch Backend started and listening")
		log.Fatal(r.Run(":8080"))
	} else {
		var g errgroup.Group

		m := autocert.Manager{
			Prompt: autocert.AcceptTOS,
			Cache:  autocert.DirCache("/cert-cache"),
			HostPolicy: autocert.HostWhitelist(
				"licisearch.herokuapp.com",
				"www.licisearch.herokuapp.com",
				"wwww.buscadorlici.tech",
				"buscadorlici.tech",
				"licimatic-search.now.sh",
				"www.licimatic-search.now.sh",
				"licimatic-search.emedinar11.now.sh",
				"www.licimatic-search.emedinar11.now.sh",
				"https://licimatic-search.emedinar11.now.sh/",
				"ZEIT",
				"zeit",
				"zeit.world",
				".buscadorlici.tech",
				"*.buscadorlici.tech",
				"*",
				"app.buscadorlici.tech",
				"lici.buscadorlici.tech",
				"lici.buscadorlici.de",
				"www.buscadorlici.de",
				"buscadorlici.de",
				"co.buscadorlici.tech",
				"b.zeit-world.co.uk",
				"c.zeit-world.org",
				"d.zeit-world.com",
				"e.zeit-world.net"),
		}

		g.Go(func() error {
			return http.ListenAndServe(":80", m.HTTPHandler(http.HandlerFunc(redirect)))
		})
		g.Go(func() error {
			serve := &http.Server{
				Addr:    ":443",
				Handler: r,
				TLSConfig: &tls.Config{
					GetCertificate: m.GetCertificate,
					// NextProtos:     []string{"http/1.1"}, // disable h2 because Safari :(
				},
			}
			return serve.ListenAndServeTLS("", "")
		})
		log.Println("LiciSearch Backend started and listening on port 8080/8443")

		log.Fatal(g.Wait())
	}

}

func search(c *gin.Context) {

	word := c.Query("word")
	filterstart := c.Query("filterstart")
	filterend := c.Query("filterend")
	orderedBy := c.Query("orderedBy")

	fmt.Println("########WORD ", word)
	fmt.Println("########filterstart ", filterstart)
	fmt.Println("########filterend ", filterend)
	fmt.Println("########orderedBy ", orderedBy)

	var startFilter int64
	var endFilter int64

	if filterstart == "" {
		filterstart = "1"
		var err error
		startFilter, err = strconv.ParseInt(filterstart, 10, 64)
		if err != nil {
			fmt.Println("Error converions string to int: ", err)
		}
	} else {
		var err error
		startFilter, err = strconv.ParseInt(filterstart, 10, 64)
		if err != nil {
			fmt.Println("Error converions string to int: ", err)
		}
	}
	if filterend == "" {
		// filterend = "100900100200300"
		filterend = "100600500400300"
		var err error
		endFilter, err = strconv.ParseInt(filterend, 10, 64)
		if err != nil {
			fmt.Println("Error converions string to int: ", err)
		}
	} else {
		var err error
		endFilter, err = strconv.ParseInt(filterend, 10, 64)
		if err != nil {
			fmt.Println("Error converions string to int: ", err)
		}
	}
	word = `%` + word + `%`
	fmt.Println()
	// if orderedBy == "higherValue" {
	// 	orderBy := "price DESC"
	// 	queryDb(startFilter, endFilter, word, orderBy, *&c)
	// } else if orderedBy == "oldDate" {
	// 	orderBy := "created_at ASC"
	// 	queryDb(startFilter, endFilter, word, orderBy, *&c)
	// } else if orderedBy == "recentDate" {
	// 	orderBy := "created_at DESC"
	// 	queryDb(startFilter, endFilter, word, orderBy, *&c)
	// } else if orderedBy == "lowerValue" {
	// 	orderBy := "price ASC"
	// 	queryDb(startFilter, endFilter, word, orderBy, *&c)
	// } else {
	// 	orderBy := "created_at DESC"
	// 	queryDb(startFilter, endFilter, word, orderBy, *&c)
	// }
	queryDb(startFilter, endFilter, word, orderedBy, *&c)

}

func redirect(w http.ResponseWriter, req *http.Request) {
	var serverHost = "buscadorlici.tech"
	serverHost = strings.TrimPrefix(serverHost, "http://")
	serverHost = strings.TrimPrefix(serverHost, "https://")
	req.URL.Scheme = "https"
	req.URL.Host = serverHost

	w.Header().Set("Strict-Transport-Security", "max-age=31536000")

	http.Redirect(w, req, req.URL.String(), http.StatusMovedPermanently)
}

func queryDb(startFilter int64, endFilter int64, word string, orderBy string, c *gin.Context) {
	fmt.Println("Entro QUERY")
	type Bid struct {
		ID          int       `db:"id"`
		EntityName  string    `db:"entity_name"`
		Price       int64     `db:"price"`
		Title       string    `db:"title"`
		Description string    `db:"description"`
		URL         string    `db:"url"`
		CreatedAt   time.Time `db:"created_at"`
	}

	host := `host=` + os.Getenv("DB_HOST") +
		` user=` + os.Getenv("DB_USER") +
		` password=` + os.Getenv("DB_PASSWORD") +
		` database=` + os.Getenv("DB_NAME")

	conn, err := pgx.Connect(context.Background(), host)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connection to database: %v\n", err)
		os.Exit(1)
	}

	// id, name, email, password_hash, is_confirmed, role_id, created_at, updated_at, whatsapp, premium, trial_expiration, channelwhatsappstatus, channelemailstatus, facebookprofile, channelfacebookstatus

	var rows pgx.Rows
	fmt.Println("query ", word)
	fmt.Println("query ", startFilter)
	fmt.Println("query ", endFilter)
	fmt.Println("query ", orderBy)
	// asc := "ASC"
	if word == "%%" && orderBy == "recentDate" {
		fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date
		AND (price >= $1 AND price <= $2)
		ORDER BY created_at DESC`, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if word == "%%" && orderBy == "higherValue" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date
		AND (price >= $1 AND price <= $2)
		ORDER BY price DESC`, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if word == "%%" && orderBy == "oldDate" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date
		AND (price >= $1 AND price <= $2)
		ORDER BY created_at ASC`, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if word == "%%" && orderBy == "lowerValue" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date
		AND (price >= $1 AND price <= $2)
		ORDER BY price ASC`, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if orderBy == "recentDate" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date - interval '7 day'
		AND (title ILIKE $1 OR description ILIKE $1)
		AND (price >= $2 AND price <= $3)
		ORDER BY created_at DESC`, word, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if orderBy == "higherValue" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date - interval '7 day'
		AND (title ILIKE $1 OR description ILIKE $1)
		AND (price >= $2 AND price <= $3)
		ORDER BY price DESC`, word, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if orderBy == "oldDate" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date - interval '7 day'
		AND (title ILIKE $1 OR description ILIKE $1)
		AND (price >= $2 AND price <= $3)
		ORDER BY created_at ASC`, word, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else if orderBy == "lowerValue" {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date - interval '7 day'
		AND (title ILIKE $1 OR description ILIKE $1)
		AND (price >= $2 AND price <= $3)
		ORDER BY price ASC`, word, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	} else {
		rows, err = conn.Query(context.Background(), `
		SELECT
		COALESCE(id, 0),
		COALESCE(entity_name, ''),
		COALESCE(price, 0),
		COALESCE(title, ''),
		COALESCE(description, ''),
		COALESCE(url, ''),
		COALESCE(created_at, '2010-01-01 16:24:21.252888-05')
		FROM public.tender 
		WHERE created_at >= current_date
		AND (price >= $1 AND price <= $2)
		ORDER BY created_at DESC`, startFilter, endFilter)
		if err != nil {
			fmt.Println("Error query:", err)
		}
		defer rows.Close()
	}
	var bid Bid
	var bids []interface{}
	for rows.Next() {
		err := rows.Scan(&bid.ID, &bid.EntityName, &bid.Price, &bid.Title, &bid.Description, &bid.URL, &bid.CreatedAt)
		if err != nil {
			fmt.Println("Error scan rows:", err)
		}
		bids = append(bids, bid)
	}
	c.JSON(200, gin.H{
		"bids": bids,
	})

}
