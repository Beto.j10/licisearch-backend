FROM golang:alpine


ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GIN_MODE=release \
    GO_ENV=production \
    DB_HOST=104.196.215.188 \
    DB_USER=postgres \
    DB_PASSWORD=lc90YH8t98gh54r \
    DB_NAME=main
    
WORKDIR /build

COPY go.mod .
COPY go.sum .
# COPY cert.pem .
# COPY key.pem .
RUN go mod download

COPY . .

RUN go build -o main .

WORKDIR /dist

RUN cp /build/main .
# RUN cp /build/cert.pem .
# RUN cp /build/key.pem .

EXPOSE 8080
EXPOSE 8443

CMD ["/dist/main"]
